#!/bin/bash
# update system snapshots
# determine current timestamp and details of older snapshots.
# 1) create new set of snapshots and save to file /usr/local/etc/snapshot${volumeType}_$tstamp
# 	eg. /usr/local/etc/snapshotSystem_20170101
#
# 2) get timestamp of old snaphots from file /usr/local/etc/snapshotTimeStamp_Old
#   for each of these snapshots delete them, then delete file.
#
# 3) rotate files:
#   currentTimeStamp --> snapshotTimeStamp_Old
#   newTimeStamp --> snapshotTimeStamp_Current
#
volumeType=System
tstamp=`date +%Y%m%d`
vollist=/usr/local/etc/snapshot${volumeType}Volumes

newTimestampFile=/usr/local/etc/snapshotTimeStamp_New
SnapList=/usr/local/etc/snapshot${volumeType}_$tstamp
echo $tstamp > $newTimestampFile

oldTimestampFile=/usr/local/etc/snapshotTimeStamp_Old
oldTimestamp=`cat $oldTimestampFile`
currentTimestampFile=/usr/local/etc/snapshotTimeStamp_Current
currentTimestamp=`cat $currentTimestampFile`

newSnapList=/usr/local/etc/snapshot${volumeType}_$tstamp
currentSnapList=/usr/local/etc/snapshot${volumeType}_$currentTimestamp
oldSnapList=/usr/local/etc/snapshot${volumeType}_$oldTimestamp
toDeleteSnapList=$oldSnapList

tmpf1=/tmp/snap.1.$$
export AWS_DEFAULT_OUTPUT=json
# aws ec2 describe-snapshots --filters "Name=tag-key,Values=Timestamp" "Name=tag-value,Values=20170101"
IFS=':'

# 1) take new snapshots
echo "Timestamps: old: ${oldTimestamp}, current: ${currentTimestamp}, new: ${tstamp};"
echo -n > $newSnapList
cat $vollist|while read host vol; do
 echo -n "creating snapshot: ${tstamp} ${host} ${vol} ${volumeType}"
 snapshotId=`/usr/local/bin/aws ec2 create-snapshot --volume-id $vol --output json --description "$host $volumeType $tstamp" | /usr/local/bin/jq-linux64 -r '.SnapshotId'`
 /usr/local/bin/aws ec2 create-tags --resources  $snapshotId --tags "Key=Name,Value=${host}_${volumeType}_$tstamp"
 /usr/local/bin/aws ec2 create-tags --resources  $snapshotId --tags "Key=Type,Value=backup"
 /usr/local/bin/aws ec2 create-tags --resources  $snapshotId --tags "Key=Timestamp,Value=${tstamp}"
 echo "$host:$volumeType:$snapshotId" >> $newSnapList
 echo " snapshotId: ${snapshotId}"
 sleep 60
done


# 2) delete old snapshots
echo "Deleting old: ${oldTimestamp}; snapshots: ${toDeleteSnapList};"
cat $toDeleteSnapList|while read host volumeType snapshotId; do
 echo "deleting: ${host} ${snapshotId} ${volumeType}"
 /usr/local/bin/aws ec2 delete-snapshot --snapshot-id $snapshotId
done
rm $toDeleteSnapList

# 3) rotate snapshot files 
echo "Rotating Timestamps: current: ${currentTimestamp}; ${currentTimestampFile} -> ${oldTimestampFile};"
echo "                         new: ${tstamp}; ${newTimestampFile} -> ${currentTimestampFile};"
/bin/cp $currentTimestampFile $oldTimestampFile
/bin/cp $newTimestampFile $currentTimestampFile


/bin/rm -f $tmpf1
