Snapshot backup
===============
Snapshot backup is a bash script using the AWS Cli to backup a set of EC2 Volumes.
Driven by a text file in /usr/local/etc/snapshotSystemVolumes
containing:

>  <hostname>:<volume-id>

for example:

    web1:vol-345881f0
    dns-2:vol-715c5365
    dns-1:vol-425c5356
    web2:vol-c26bb906

Prerequisities/Requirements
===========================
This script uses the following:

* aws [cli tools](http://docs.aws.amazon.com/cli/latest/userguide/installing.html) 
* json parser - [jq-linux](https://stedolan.github.io/jq/)

Operation
=========
Designed to be run from cron, with AWS credentials already configured.
While a run is in progress and there snapshots being created there will be 3 sets of snapshots:

* Old - about to be deleted
* Current
* New - in the process of creation.

Each set of snapshots is timestamped with the current date.
The roll-over doesn't support running more than once a day.
For each run a text file is generated in /usr/local/etc:

    root@hosting [/usr/local/etc]# cat snapshotSystem_20170101
    web2:System:snap-0348d71e77f456e3b
    dns-2:System:snap-0345c5405046c692d
    dns-1:System:snap-0dc5804d4b21a1da5
    web2:System:snap-0a5256a2cccb91fd1

In addition there are pointers to these files:

    root@hosting [/usr/local/etc]# more snapshotTimeStamp*
    ::::::::::::::
    snapshotTimeStamp_Current
    ::::::::::::::
    20170102
    ::::::::::::::
    snapshotTimeStamp_New
    ::::::::::::::
    20170102
    ::::::::::::::
    snapshotTimeStamp_Old
    ::::::::::::::
    20170101